import sys
from collections import deque

class Lexer:

    WHITESPACE = (' ', '\t', '\n')
    LITERAL = ('+', '-', '*', '/', '(', ')')
    LETTER = ('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z')
    DIGIT = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
    STATE = {
        "START": 0,
        "IDEN": 1,
        "CONST": 2,
        "DECIMAL_DOT": 3,
        "DECIMAL": 4,
    }
    STATE_REVERSE = {
        0: "START",
        1: "IDEN",
        2: "CONST",
        3: "DECIMAL_DOT",
        4: "DECIMAL",
    }
    TOKEN = {
        "EOF": 0,
        "ERROR": 1,
        "LITERAL": 2,
        "IDEN": 3,
        "CONST": 4,
    }
    TOKEN_REVERSE = {
        0: "EOF",
        1: "ERROR",
        2: "LITERAL",
        3: "IDEN",
        4: "CONST",
    }

    def __init__(self):
        self.__input = deque('')
        self.__lexeme = ''
        self.__output = []
        self.__state = Lexer.STATE["START"]

    def __getInputOne(self):
        return self.__input.popleft()

    def __addOutput(self, token, lexeme):
        #print('{}\t{}'.format(Lexer.TOKEN_REVERSE[token], lexeme))
        self.__output.append((token, lexeme))
        self.__lexeme = ''

    def printState(self):
        print('# STATE: {}'.format(Lexer.STATE_REVERSE[self.__state]))

    def analysis(self, input):
        self.__input = deque(input)
        self.__lexeme = ''
        self.__output = []
        while self.__input:
            if self.__state == Lexer.STATE["START"]:
                char = self.__getInputOne()
                if char in Lexer.WHITESPACE:
                    pass
                elif char in Lexer.LITERAL:
                    self.__addOutput(Lexer.TOKEN["LITERAL"], char)
                elif char in Lexer.LETTER:
                    self.__lexeme += char
                    self.__state = Lexer.STATE["IDEN"]
                elif char in Lexer.DIGIT:
                    self.__lexeme += char
                    self.__state = Lexer.STATE["CONST"]
                else:
                    self.__addOutput(Lexer.TOKEN["ERROR"], char)
            elif self.__state == Lexer.STATE["IDEN"]:
                char = self.__getInputOne()
                if char in (Lexer.LETTER + Lexer.DIGIT):
                    self.__lexeme += char
                elif char in Lexer.LITERAL:
                    self.__addOutput(Lexer.TOKEN["IDEN"], self.__lexeme)
                    self.__addOutput(Lexer.TOKEN["LITERAL"], char)
                    self.__state = Lexer.STATE["START"]
                elif char in Lexer.WHITESPACE:
                    self.__addOutput(Lexer.TOKEN["IDEN"], self.__lexeme)
                    self.__state = Lexer.STATE["START"]
                else:
                    self.__addOutput(Lexer.TOKEN["IDEN"], self.__lexeme)
                    self.__addOutput(Lexer.TOKEN["ERROR"], char)
                    self.__state = Lexer.STATE["START"]
            elif self.__state == Lexer.STATE["CONST"]:
                char = self.__getInputOne()
                if char in Lexer.DIGIT:
                    self.__lexeme += char
                elif char in Lexer.LETTER:
                    self.__addOutput(Lexer.TOKEN["CONST"], self.__lexeme)
                    self.__lexeme = char
                    self.__state = Lexer.STATE["IDEN"]
                elif char in Lexer.LITERAL:
                    self.__addOutput(Lexer.TOKEN["CONST"], self.__lexeme)
                    self.__addOutput(Lexer.TOKEN["LITERAL"], char)
                    self.__state = Lexer.STATE["START"]
                elif char in Lexer.WHITESPACE:
                    self.__addOutput(Lexer.TOKEN["CONST"], self.__lexeme)
                    self.__state = Lexer.STATE["START"]
                elif char == '.':
                    self.__lexeme += char
                    self.__state = Lexer.STATE["DECIMAL_DOT"]
                    pass
                else:
                    self.__addOutput(Lexer.TOKEN["CONST"], self.__lexeme)
                    self.__addOutput(Lexer.TOKEN["ERROR"], char)
                    self.__state = Lexer.STATE["START"]
            elif self.__state == Lexer.STATE["DECIMAL_DOT"]:
                char = self.__getInputOne()
                if char in Lexer.DIGIT:
                    self.__lexeme += char
                    self.__state = Lexer.STATE["DECIMAL"]
                else:
                    self.__addOutput(Lexer.TOKEN["ERROR"], self.__lexeme)
                    if char in Lexer.LETTER:
                        self.__lexeme += char
                        self.__state = Lexer.STATE["IDEN"]
                    elif char in Lexer.LITERAL:
                        self.__addOutput(Lexer.TOKEN["LITERAL"], char)
                        self.__state = Lexer.STATE["START"]
                    elif char in Lexer.WHITESPACE:
                        self.__state = Lexer.STATE["START"]
                    else:
                        self.__addOutput(Lexer.TOKEN["ERROR"], char)
                        self.__state = Lexer.STATE["START"]
            elif self.__state == Lexer.STATE["DECIMAL"]:
                char = self.__getInputOne()
                if char in Lexer.DIGIT:
                    self.__lexeme += char
                else:
                    self.__addOutput(Lexer.TOKEN["CONST"], self.__lexeme)
                    if char in Lexer.LETTER:
                        self.__lexeme += char
                        self.__state = Lexer.STATE["IDEN"]
                    elif char in Lexer.LITERAL:
                        self.__addOutput(Lexer.TOKEN["LITERAL"], char)
                        self.__state = Lexer.STATE["START"]
                    elif char in Lexer.WHITESPACE:
                        self.__state = Lexer.STATE["START"]
                    else:
                        self.__addOutput(Lexer.TOKEN["ERROR"], char)
                        self.__state = Lexer.STATE["START"]
        # if input is EOF(empty):
        if self.__state == Lexer.STATE["IDEN"]:
            self.__addOutput(Lexer.TOKEN["IDEN"], self.__lexeme)
        elif self.__state == Lexer.STATE["CONST"]:
            self.__addOutput(Lexer.TOKEN["CONST"], self.__lexeme)
        elif self.__state == Lexer.STATE["DECIMAL_DOT"]:
            self.__addOutput(Lexer.TOKEN["ERROR"], self.__lexeme)
        elif self.__state == Lexer.STATE["DECIMAL"]:
            self.__addOutput(Lexer.TOKEN["CONST"], self.__lexeme)

        # Prepare output for return
        output_copy = self.__output.copy()
        self.__output = []
        return output_copy
                
def main():
    infile = sys.stdin
    myLexer = Lexer()
    output = myLexer.analysis(infile.read())
    for i in output:
        print('{}\t{}'.format(Lexer.TOKEN_REVERSE[i[0]], i[1]))

if __name__ == "__main__":
    main()